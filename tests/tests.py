import pytest
from keycloakauth.keycloak import KeycloakWorker, KeycloakException
import time


class MockResponse:
    def __init__(self, json, status):
        self._json = json
        self.status = status

    def __enter__(self):
        return self

    def json(self):
        return self._json

    def read(self):
        return self._json

    def text(self):
        return self._json

    def __exit__(self, exc_type, exc, tb):
        pass


@pytest.fixture
def token():
    """
    For future use
    a jwt example: encoded and decoded
    """
    jwt = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJzd0M1WjlGUWtJUHp5ZlR4ZWhqMnZkXzFSVFhyUTlPQ2xuZHhqNnFBc1JjIn0.eyJleHAiOjE2MDQ5OTE1NzMsImlhdCI6MTYwNDk5MTI3MywianRpIjoiMWZhZTVlNmMtNzBlYy00NDJhLTg4M2ItYWZlN2YyMzQ1MDU3IiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay1kc28tY2xvdWQuYXBwcy5kMC1vc2NwLmNvcnAuZGV2LnZ0Yi9hdXRoL3JlYWxtcy9OYW1pbmciLCJhdWQiOlsiYWNjb3VudCIsIm5hbWluZ2hhdCJdLCJzdWIiOiIxYTIzMzYzNC02ODIzLTQzOGEtOTU4Ny03YTQ3ZTNiY2JiMTIiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJuYW1pbmctZnJvbnQiLCJzZXNzaW9uX3N0YXRlIjoiNjE3Y2E2MzktZWZkZS00ZjU3LWFjZjgtNjEzZDhlYjZkZTI3IiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyJodHRwOi8vIiwiaHR0cDovLzEwLjIwMy4zMi4xMzkiLCJodHRwOi8vMTI3LjAuMC4xOjgwODAiLCJodHRwOi8vMTI3LjAuMC4xOjMwMDAiLCJodHRwczovLzEyNy4wLjAuMTo4MDQzIiwiaHR0cHM6Ly9pZnNvdWwtYXAyMDA0bG4uY29ycC5kZXYudnRiIiwiaHR0cDovLzEyNy4wLjAuMSIsImh0dHBzOi8vMTI3LjAuMC4xIiwiaHR0cHM6Ly8xMC4yMDMuMzIuMTM5IiwiaHR0cDovLzEyNy4wLjAuMTo4MDAwIiwiaHR0cDovL2tleWNsb2FrLWRzby1jbG91ZC5hcHBzLmQwLW9zY3AuY29ycC5kZXYudnRiIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19LCJuYW1pbmdoYXQiOnsicm9sZXMiOlsiYmFuazp1c2VyOm1hbmFnZSIsImJhbms6YWxsOm1hbmFnZSJdfX0sInNjb3BlIjoib3BlbmlkIGVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsIm5hbWUiOiJHdXN0YXYgS2xpbXQiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJrbGltdCIsImdpdmVuX25hbWUiOiJHdXN0YXYiLCJmYW1pbHlfbmFtZSI6IktsaW10IiwiZW1haWwiOiJrbGltdEB2dGIucnUifQ.rmRpEHnNqrjkbrfjvWWthI9nbeT2nMGqUkr1Ki4JWxlaqGIwT3mOAycI6uku1gAAm6UJQbvG1qc4K3hgozA_mXQsn6b4lDvje3Tjlg6Y1eMV52OSqazhA82vIMmLp3_dDLOc0pDf7FHeOWlvQG2gPRYzVHFy7j76sRdmRsNNv8mqRk2XTpAHko5jNG7pKtGF6M79sq8exvr_SI9XEuKKDZyK-gLcaoRpg5GKnFnyfBdw_oRvJ7onlUIPACz3DJJdTlja20jlmbaR0YYbnH1uoGX1l_cWmR7GsphLlNjnMg8xZcZADIW2eIZilHtBI4awRliDQAhoWsLoSnc1vNhVbA"
    header = {
        "alg": "RS256",
        "typ": "JWT",
        "kid": "swC5Z9FQkIPzyfTxehj2vd_1RTXrQ9OClndxj6qAsRc"
    }
    token = {
        "exp": 1604991573,
        "iat": 1604991273,
        "jti": "1fae5e6c-70ec-442a-883b-afe7f2345057",
        "iss": "https://keycloak-dso-cloud.apps.d0-oscp.corp.dev.vtb/auth/realms/Naming",
        "aud": [
            "account",
            "naminghat"
        ],
        "sub": "1a233634-6823-438a-9587-7a47e3bcbb12",
        "typ": "Bearer",
        "azp": "naming-front",
        "session_state": "617ca639-efde-4f57-acf8-613d8eb6de27",
        "acr": "1",
        "allowed-origins": [
            "http://",
            "http://10.203.32.139",
            "http://127.0.0.1:8080",
            "http://127.0.0.1:3000",
            "https://127.0.0.1:8043",
            "https://ifsoul-ap2004ln.corp.dev.vtb",
            "http://127.0.0.1",
            "https://127.0.0.1",
            "https://10.203.32.139",
            "http://127.0.0.1:8000",
            "http://keycloak-dso-cloud.apps.d0-oscp.corp.dev.vtb"
        ],
        "realm_access": {
            "roles": [
                "offline_access",
                "uma_authorization"
            ]
        },
        "resource_access": {
            "account": {
                "roles": [
                    "manage-account",
                    "manage-account-links",
                    "view-profile"
                ]
            },
            "naminghat": {
                "roles": [
                    "bank:user:manage",
                    "bank:all:manage"
                ]
            }
        },
        "scope": "openid email profile",
        "email_verified": False,
        "name": "Gustav Klimt",
        "preferred_username": "klimt",
        "given_name": "Gustav",
        "family_name": "Klimt",
        "email": "klimt@vtb.ru"
    }
    return jwt


@pytest.fixture
def config():
    return {
        'KEYCLOAK_SERVER_URL': 'http://localhost/auth',
        'KEYCLOAK_REALM': 'master',
        'KEYCLOAK_CLIENT_ID': 'client-back',
        'KEYCLOAK_CLIENT_SECRET_KEY': '12386724-5789-1234-1234-12345678987',
        'KEYCLOAK_TECH_USER': 'admin',
        'KEYCLOAK_TECH_PASSWORD': 'admin',
    }


@pytest.fixture
def auth_data():
    return {"access_token": "mock_token",
            "refresh_token": "mock_r_token",
            'expires_in': 300,
            'refresh_expires_in': 600}


@pytest.fixture
def _id():
    return {"id": "111"}


@pytest.fixture
def _mock():
    return {"mock": True}


@pytest.fixture
def kworker(config):
    verify = False
    return KeycloakWorker(config=config, verify=verify)


@pytest.fixture
def request_mock_auth(kworker, auth_data, requests_mock):
    requests_mock.post(kworker.server_url + kworker.token_endpoint, json=auth_data)
    return requests_mock


def test_get_token(kworker, auth_data, requests_mock):
    requests_mock.post(kworker.server_url + kworker.token_endpoint, json=auth_data, status_code=500)
    with pytest.raises(KeycloakException):
        kworker.get_token(auth_data=kworker.auth_data)

    requests_mock.post(kworker.server_url + kworker.token_endpoint, json=auth_data)
    kworker.get_token(auth_data=kworker.auth_data)
    assert kworker.token == "mock_token"
    assert kworker.refresh_token == 'mock_r_token'
    assert kworker.token_expiration == time.time() + 300
    assert kworker.refresh_token_expiration == time.time() + 600


def test_get_group(kworker, requests_mock, _mock, request_mock_auth):
    requests_mock.get(kworker.server_url + kworker.groups_endpoint, status_code=500)
    with pytest.raises(KeycloakException):
        kworker.get_group(is_code='bank', name_type="hostname")

    requests_mock.get(kworker.server_url + kworker.groups_endpoint, json=_mock)
    result = kworker.get_group(is_code='bank', name_type="hostname")
    assert result.get("mock") is True


def test_get_group_members(kworker, requests_mock, _mock, _id, request_mock_auth):
    requests_mock.get(kworker.server_url + kworker.groups_endpoint, status_code=500)
    with pytest.raises(KeycloakException):
        kworker.get_group_members(is_code='bank', name_type="hostname")

    requests_mock.get(kworker.server_url + kworker.groups_endpoint, json=[])
    result = kworker.get_group_members(is_code='bank', name_type="hostname")
    assert result == {'error': 'Group not found'}

    requests_mock.get(kworker.server_url + kworker.groups_endpoint, json=[_id])
    requests_mock.get(kworker.server_url + kworker.groups_endpoint + '/111/members', json=_mock)
    result = kworker.get_group_members(is_code='bank', name_type="hostname")
    assert result.get("mock") is True


def test_get_user(kworker, requests_mock, _mock, request_mock_auth):
    requests_mock.get(kworker.server_url + kworker.users_endpoint, status_code=500)
    with pytest.raises(KeycloakException):
        kworker.get_user(username='klimt')

    requests_mock.get(kworker.server_url + kworker.users_endpoint, json=_mock)
    result = kworker.get_user(username='klimt')
    assert result.get("mock") is True


def test_add_user_to_group(kworker, requests_mock, _id, request_mock_auth):
    requests_mock.get(kworker.server_url + kworker.user_group_endpoint, status_code=500)
    with pytest.raises(KeycloakException):
        kworker.add_user_to_group(is_code='bank', username='klimt')

    requests_mock.get(kworker.server_url + kworker.users_endpoint, json=[])
    result = kworker.add_user_to_group(is_code='bank', username='klimt')
    assert result == {'error': 'User not found'}

    requests_mock.get(kworker.server_url + kworker.users_endpoint, json=[_id])
    requests_mock.get(kworker.server_url + kworker.groups_endpoint, json=[])
    result = kworker.add_user_to_group(is_code='bank', username='klimt')
    assert result == {'error': 'Group not found'}

    requests_mock.get(kworker.server_url + kworker.users_endpoint, json=[_id])
    requests_mock.get(kworker.server_url + kworker.groups_endpoint, json=[_id])
    requests_mock.put(kworker.server_url +
                      kworker.user_group_endpoint % {'user_uuid': _id.get('id'), 'group_uuid': _id.get('id')},
                      status_code=200)
    result = kworker.add_user_to_group(is_code='bank', username='klimt')
    assert result.get("success") is True


def test_remove_user_to_group(kworker, requests_mock, _id, request_mock_auth):
    requests_mock.get(kworker.server_url + kworker.user_group_endpoint, status_code=500)
    with pytest.raises(KeycloakException):
        kworker.remove_user_from_group(is_code='bank', username='klimt')

    requests_mock.get(kworker.server_url + kworker.users_endpoint, json=[])
    result = kworker.remove_user_from_group(is_code='bank', username='klimt')
    assert result == {'error': 'User not found'}

    requests_mock.get(kworker.server_url + kworker.users_endpoint, json=[_id])
    requests_mock.get(kworker.server_url + kworker.groups_endpoint, json=[])
    result = kworker.remove_user_from_group(is_code='bank', username='klimt')
    assert result == {'error': 'Group not found'}

    requests_mock.get(kworker.server_url + kworker.users_endpoint, json=[_id])
    requests_mock.get(kworker.server_url + kworker.groups_endpoint, json=[_id])
    requests_mock.delete(kworker.server_url +
                         kworker.user_group_endpoint % {'user_uuid': _id.get('id'), 'group_uuid': _id.get('id')},
                         status_code=204)
    result = kworker.remove_user_from_group(is_code='bank', username='klimt')
    assert result.get("success") is True


def test_get_client_uuid(kworker, requests_mock, _id, request_mock_auth):
    requests_mock.get(kworker.server_url + kworker.clients_endpoint, status_code=500)
    with pytest.raises(KeycloakException):
        kworker.get_client_uuid()

    requests_mock.get(kworker.server_url + kworker.clients_endpoint, json=[_id])
    kworker.get_client_uuid()
    assert kworker.client_uuid == '111'


def test_get_role(kworker, requests_mock, _mock, _id, request_mock_auth):
    requests_mock.get(kworker.server_url + kworker.roles_endpoint, status_code=500)
    with pytest.raises(KeycloakException):
        kworker.get_role(is_code='bank', name_type="hostname")

    requests_mock.get(kworker.server_url + kworker.clients_endpoint, json=[_id])
    kworker.get_client_uuid()
    requests_mock.get(kworker.server_url + kworker.roles_endpoint % {'client_uuid': kworker.client_uuid}, json=_mock)
    result = kworker.get_role(is_code='bank', name_type="hostname")
    assert result.get("mock") is True


def test_create_role(kworker, requests_mock, _id, request_mock_auth):
    requests_mock.get(kworker.server_url + kworker.roles_endpoint, status_code=500)
    with pytest.raises(KeycloakException):
        kworker.create_role(is_code='bank', name_type="hostname")

    requests_mock.get(kworker.server_url + kworker.clients_endpoint, json=[_id])
    kworker.get_client_uuid()
    requests_mock.get(kworker.server_url + kworker.roles_endpoint % {'client_uuid': kworker.client_uuid},
                      json=[{"name": "bank:hostname:manage "}])
    create_mock = requests_mock.post(kworker.server_url + kworker.roles_endpoint % {'client_uuid': kworker.client_uuid},
                                     status_code=201)
    kworker.create_role(is_code='bank', name_type="hostname")
    assert create_mock.called
    assert create_mock.call_count == 1


def test_create_group(kworker, requests_mock, request_mock_auth):
    requests_mock.get(kworker.server_url + kworker.groups_endpoint, status_code=500)
    with pytest.raises(KeycloakException):
        kworker.create_group(is_code='bank', name_type="hostname")

    requests_mock.get(kworker.server_url + kworker.groups_endpoint,
                      json=[{"name": "bank_hostname_manage"}])
    assert kworker.create_group(is_code='bank', name_type="hostname") is None

    requests_mock.get(kworker.server_url + kworker.groups_endpoint, json=[])
    create_mock = requests_mock.post(kworker.server_url + kworker.groups_endpoint, status_code=201)
    kworker.create_group(is_code='bank', name_type="hostname")
    assert create_mock.called
    assert create_mock.call_count == 1


def test_create_role_mapping(kworker, requests_mock, _id, _mock, request_mock_auth):
    requests_mock.get(kworker.server_url + kworker.role_group_mapping_endpoint, status_code=500)
    with pytest.raises(KeycloakException):
        kworker.create_role_mapping(is_code='bank', name_type="hostname")

    requests_mock.get(kworker.server_url + kworker.groups_endpoint,
                      json=[{"name": "bank_hostname_manage", "id": "111"}])
    requests_mock.get(kworker.server_url + kworker.clients_endpoint, json=[_id])
    kworker.get_client_uuid()

    requests_mock.get(kworker.server_url + kworker.roles_endpoint % {'client_uuid': kworker.client_uuid},
                      json=[{"name": "bank:hostname:manage "}])

    requests_mock.post(kworker.server_url +
                       kworker.role_group_mapping_endpoint % {'client_uuid': kworker.client_uuid, 'group_uuid': '111'},
                       json=_mock)
    result = kworker.create_role_mapping(is_code='bank', name_type="hostname")
    assert result.get("success") is True


def test_userinfo(kworker, token, _mock, requests_mock):
    requests_mock.get(kworker.userinfo_endpoint, json=_mock)
    result = kworker.userinfo(token=token)
    assert result.get("mock") is True


def test_introspect(kworker, token, _mock, requests_mock):
    requests_mock.post(kworker.token_introspection_endpoint, json=_mock)
    result = kworker.introspect(token=token)
    assert result.get("mock") is True


def test_well_known(kworker, _mock, requests_mock):
    requests_mock.get(kworker.well_known_endpoint, json=_mock)
    result = kworker.well_known()
    assert result.get("mock") is True
